-----------------------------------------------------------------------------------
A Solemn Discourse Upon the Grand Covenant,

Opening
The Divinity and Policy of it:

By John Salmarth
Master of Art's, and not long since, Pastor of
Heflerson in Yorkshire.

London, Printer fro Laurence Blaiklock. 1643
-----------------------------------------------------------------------------------

The Printer to the Reader:

Courteous Reader, The Essay's of this Emblem should have been cut, but that time could not permit.

An Angel reaching forth an hand out of a cloud, holding a chain, which is let down to three women, each one having her arm linked in it, over each a Title: Anglia, Scotia, Hibernia, with Harpes in their hands, at the distance of a stream betwixt another woman with the title of Roma in a sad posture, her Triple Crown seeming to decline with her fact towards the three; with this Inscription, Revel. 17:1:

"And there came one of the seven Angels, which had the seven vials and talked with me, saying unto me, Come hither, and I will show unto  thee the judgment of the great Shore that sitteth upon the waters."


To the Worthy Covenanter,

   I could have given more, and more east and common observations; but the times call for discourses that work higher, and more quaintly; I say not this to force any reputation upon the notions. 
   This Covenant is of a Reformation in any age; mighty and powerful are the principles of it: and though this season of our solemnity be cloudy and our evening bloody, yet is our Savior's interpretation, "

"When it is evening, you say it will be fair weather, for the sky is red."

Holy Obligatory Principles

Preamble

Having before our eyes the Glory of God, and each one of us for himself with our hand's lifted up to the most high God.

Article 6

And this Covenant we do make in the presence of Almighty God, the searcher of all hearts. As we shall answer at that great day, when the secrets of all hearts shall be disclosed.

The Discourse

The Power and Excellency of this Covenant 

A Covenant is the last resort of the godly and wise Christian; and with this, he draws himself nearer to Heaven, and closer to that glorious Essence, and the immediate flowings emanation of an Almighty power; A Covenant is such an obligation, as lays an engagement upon the soul; and as in tossings and storms at sea, the Mariners find out no safer course than by casting out Cords and Anchors, to hold them at some Period; so in civil waves and spiritual fluctuations, there cannot be a more secure experiment, then this of Covenanting, which is like the asting out our Cord and Anchors as they did in St. Paul's storm, under girding the ship, and casting out four Anchors, Covenants, they are Divine Engines, which the godly have found out to wind up their fouls from irregular wanderings and strayings, into heavenly heights and stations, the only remedy, and preventative againsts relapses, and Apostacies; and those virtues and operations the Saints have ever found in Covenants: for such resolutions of souls are but the finer cordage, which the Spirit spins out and twists from the substance of its own effence; and now God and his Angels have something to hold our souls by, even the operations and reflexes of our own spirits; and though God need none of those, but is able to keep us up by the immediate and and indistant workings of his holdy Spirit, yet he is a God that is pleased to take us at our own rebound, and to admit us into that holy consolation, "We as workers together with him."
  He that covenants with God, by that very act does carry up himself unto God's throne, and sites his soul to his Tribunal, and then the majesty of God looks on him with a fuller gleam; and so long as that glorious interview contues, or any sparkling or saying of it, man is awed from sinning and stands trembling like the people of Israel, while God appeared upon the Mount. 
  And thus Divine Covenants, as they exalt and situate a soul in more glory then before, even in the glorious face of God, so they are spiritual stays and supports, and strengthenings of a soul.
  God himself first drew forth hiw own essence into this course of Covenants to Abraham and Moses, and Joshua, and his people, and from that Covenant he went higher, to one of gract, besides the particular obligations of his, to Noah & other Saints, not as if he received any consolation or consortion by it, or any such act of covenanting that he had not before; not as if he begun to react upon himself in any new operation (far be it from his immutable essence) he was as firm and unchangeable in the eternal immanency of his own, before ever he passed himself abroad into any such act of passion: and therefor he could not show himself to man in the likeness of any other notion, than, "I am."
  Only he was pleased to light us by a beam of his own nature, into this duty of holy confederation, and to show us a new way of spiritual advancement and establishment: how sacred then and how inviolable ought these to be which are made with a most high God; when even passions and promises and Covenants in friendships and lower confederations, are reputed holy. 
  Thus far of the power of the covenant upon the soul in that grant and heavenly engagement; now there is a power reflexive, and that is a return it makes from Heaven, and in that return it brings with it something of God; for the soul going up thither by a spiritual might and holy violence, brings away from thence graces and blessings, and the resort of many temporal mercies as when Moses had been looking God in the face, he brought a divine luster upon his own, home with him.
  We see Nehemiah's covenant bad excellent commitments, the dedication was kept with gladness, and singing and Psalteries, and the people offered themselves willingly, and the business at Jerusalem, and all of the affairs of God's house went better on in all the particulars. Nehem. II: 3,12,27. 
  The covenant of Judah drew along with it the like blessings; the Lord was found of them and gave them rest; and one more superlative blessing not inconsistent with our calamity, Macchah was removed from being Queen, because she had made an idol in a grove, 2 Chron 5:25-26.
  And for this covenant of ours, I am bold to say, it hath been in heaven already; it came not only from thence in its first inspiration, but it hath had a return back, and by the power of that reflexive act, it hath brought down with it cheerful concurrencies and contributions in both kingdoms, and there are divine stirrings, and movings, and aspirations in the people of late; and as in the Pool of Bethseda, the stirrings and troubles in the waters were the only sign of the Angels coming down; So these waters in both kingdoms, which in the Holy Spirit language are people, doth stir and move more of late, not only in their highest and supreme representative, but in their own places, which is an indication of Divine virtue, defended and co-operating.

Covenant - The Reformation Principles

Preamble

I. To endeavor the advancement of the kingdom of Jesus Christ.

Article I.

The reformation of religion in doctrine, worship, and discipline, according to the word of God and the example of the best churches. 

Art. 6

The unfeigned desire to be humbled for our sins, and the sins of these kingdoms. 

Discourse

These are such maxims as will make a kingdom holy and happy; for holiness is the foundation and basis to all other blessings, and hath a perpetuating quality, and it is such a condition as God takes in at the felicitating of a person or people; "Seek first the kingdom of God, and all things shall be added unto you." Matt 6:33
      The advancement of God's kingdom, was always the advancement of the kingdom of Israel; and the glory of the one declined and set in the delincation of the other; for the ark and the glory departed together, and both expired at once in a doleful Ichabod. 
      The advancement of Christ's kingdom hath been the design of God from all eternity, and it is the design of the godly too; "God revealeth his secrets to this servants, and we have the mind of Christ."

 Now knowing so much of the councils, and designs, and secrets, and mind of God, we are carried on by the same Spirit to be aiding to that design; now though the kingdom of Christ be such a name as imports glory and dominion, yet it is not aglory of this world, but a spiritual glory seated in bare and simple administrations; such as are foolishness to the Greek, and to the Jews a Rock of offense; and this kingdom of Christ like other monarchies, hath its rise and growth, its ages and improvements, according to the prophetical latitude, being at no fullness nor perfection till the rest of the monarchies be consumed before it; this is that small ex-cresency advancing out of the Mountain,and by a power insensible exalting itself through all oppositions, working through Atheism's, Paganisms, Idolatries, and Superstitions, Persecutions, and all of the carnal imaginations, into a lustre glorious, in the judgment of those only "who can spiritually discern"; the advancement of this kingdom is only attainable by reformation to the word of God; and here we shall take occasion to part with all the models and idea's which are not to be found in the holy Scriptures; it is marked there as a grand transgression to walk after "the imagination of our own hearts"; and that was laid to "Jeroboam's charge", that his priests, and Sabbaths, and worship were such as "he devised in his own heart." God will endure no such rivals nor conjunctures with himself; it is an incompletion to the work of God to build his house with our own timber; and as he made this world as first only after the pattern of his own counsels, so in this second work of the setting up a spiritual structure for his glory; God thinks none worth of coordination, in those things wherein his glory shall be sure to suffer in a distribution with his creatures, and his own image hath ever pleased him best, and therefore he made the best piece of his creation according to it. 

 And where this kingdom of Christ is, there is holiness of doctrine, holiness of government, holiness of ordinances, holiness of life; God hath had a people at first whom he made his own, by special adoption, by eminent privileges, by rare providences, by laws and institutions, by worship and administrations.
 And now because "darkness in part is happened to Israel", God will still have a people that shall be his, and have their laws and usages and forms from him; their guidance & providences from him.
 This kingdom of Christ is a company of godly gathered by his own Spirit, having their Lord and Savior in the midst, confederated by an holy and sacramental passion, ruled by the law of his will and Spirit; obeying his command whether in silent inspirations or lower exhortations, either by "a word behind us", and a saying, "See ye my mace"; or by outward intimations and interpretations of his will, from such ways of distribution and administration as he hath ordained; studying what will adorn the Gospel of Jesus Christ, and those that walk in the light and glory of it, "Being transformed from gory to glory, as by the Spirit of the Lord."
 Now these principles, viz. The advancement of Christ's kingdom, and humiliation for sins, are such as will bring God into this Kingdom, and seat him and determine his presence, and as the Tabernacle and Ark and Temple were the engagements and enthronizations of and mysterious fixations of God's divinity. 
 They are likewise an advancement of the nation too, setting it higher than other nations, that was the preferment of the Jews, "That to them the Oracles of God were committed." Therefor they were said to dwell in the light, when other nations "sat in the Region and shadow of death."
 And that principle of reformation according to the Word of God and the best reformed Churches, brings us closer to God, and consolidates us with heaven, and makes us arrive at the highest mystery, even the "denial of ourselves", our own inventions, will-worship, and superstitions; pulling down at once all our relations to "Rome" and Popery, and working to that pattern, so "That things which shall be seen shall not be made of things which do appear."; we shell like wife be associated to the Church of Christ, and so incorporated more clearly and purely and mystically, into the body of Christ, we shall be now in a capacity with them to partake equally of graces and privileges; and thus the kingdom of Christ gathers power and latitude , and stretches to the breadth of that prophecy, "of the fullness of the Gentiles," and gathers strength against the present anti-christian monarchy; and by that other principle of humiliation, we obtain the qualification and condition for mercy and peace, we approach into terms of reconciliation with God; if the "wicked forsake his way the unrighteous man his thoughts, God will forgive and abundantly pardon." What is that thickens and clouds over us, but the evaporation and exhalation of our sins and iniquities, "For your iniquities have turned away these things, and your sins have withheld good things for you."

Covenant
---------
The Principle of Expiation

In Preamble

Calling to mind the treacherous and bloody plots, conspiracies, attempts, and practices of the enemies of God. 

Article 2

That we shall in like manner endeavor without respect of persons the expiation of Popery and Prelacy. 

Discourse

Here lies the prophetical power of it against of Church of Rome, or Anti-Christian monarchy, and the bloody plots and conspiracies are such fresh remembrances, as seal us to strong and perpetual en devours; we can read in the leafs of our former ages their conspiracies still in red letters, and at this day we have succession of their bloody designs, and I conceive the rise we take from their own foundations in blood, is but in holy parallel to him, "into whole remembrance great Babylon came to give unto her the cup of the wine of the fierceness of wrath"; and to his own peoples resolution, "Happy shall he be that rewardeth thee as thou hast served use;" and the treacheries and tyrannies of God's enemies have every drawn along with them this resolution in God's people; "Amalek, and Ashur, and Egypt," are standing examples of Divine revenge. 

     And for Extirpation, it is but a retaliation to their own just cruelty, who would raise out the name of "Israel, that it should no more in remembrance."

     And what hath their endeavor in our kingdom of Ireland been, but an eradication of our memories. 

     And our confederations now, cannot but be powerful in the very notion; National Leagues have brought forth great effects in states and kingdoms, in the mutual aidings and assistings; and therefore Isreal sent to Syria, and Judah to Assyria, and Judah to Israel, and other kingdoms have fought to one another for such combinations, in any grand enterprise; That famous design of Christendom, which was such a universal confederation again the Rurk, only it was a design in a wrong channel; Therefor the more spiritual that Leagues are, the more powerful; that which Israel so famous in their conquests to Canaan, was the Association of the Tribes, and the "Ark of God amongst them, therefore the Philistines cried out, "Woe unto us, the Ark of God is in the camp of the Hebrews"; and when they marched with the Ark amongst the Jordan was driven back, and the mountains and little hills were removed, the walls of Jericho fell; And certainly this grand and blessed Association of the three kingdoms, is a glorious portent to the destruction of Rome itself, carrying amongst them such an Ark and Gospel; this is the first time that every the Sun saw such a Triple Confederation against the Triple Crown, so many states, so solemnly combined against the Popish hierarchy; this is the time the spirit of God hath set up a Standard; this is a fair rite, and improvement to the prophecy, "When the Princes of the earth shall gather themselves together, and shall agree to make her desolate."
     I know there hath been many confederations, but they were narrower than this and so, opposings, and strong ones too, and by confederacies too; yet those were but single to this, when our Princes in their eytes appeared in their gradual extirpation's, our "Henry, and Edward, and Elizabeth", when in Scotland, they against the French power and Idoltry, aided by a power from this kingdom; yet there were not from such strong resolutions, such able Principles, such a sacred Covenant; they were but ordinary and civil contributions, & weak strivings, and so had shorter expiration's, and relapses, and had little more of Religion, then brought them into the fields, and enabled them to break down images; The Confederations and Covenants in Germany, so many princes and states entering into a solemn protestation upon Luther's discoveries; The Confederations of those in Holland, and those states in the Netherlands; The Confederation of those princes in France; But these had their ebbings and flowings. But now, as if this were the Anti-christian crisis, and as if the Reformation had recovered the period of declination, three kingdoms strike into a sacred league; And now I think i will hear the Angel saying to us, "The prince of the kingdom of Persia withstood me twenty-one days, but loe Michael one of your chief Princes came in to help me, and now I am come to make you understand what shall befall in the latter days; now I think the set time to favour Zion is come, her servants take pleasure in her stones. 
     And for the Extirpation of Prelacies, though it be a governmentrivetted into our laws and usages; and into the judgments, and consciences of some, through a mistaken and colluding Divinity, yet let us not like the Jews, lose our Gospel, withholding our laws too fast; I know this kingdom hath ever been a retentive nation of customs, and old constitutions, and it parted bu sadly with its old paganism, and with its latter anti-Christianity, in the elevation of Abbie and Priories; And hence it is that Reformations in this nation, hath been with such little power and duration; for we have ever easily gone back with a new successor, never taking in so much of the power of godliness, as should be able to make us live Protestants to another succession; and especially the Superstitions and Idolotries of late, were woven with such strange and plausible Infirmations; Episcopacy was got upon the bottom of misinterpreted Scriptures; and the whole government upon a pretended antiquity; and the innovations upon a spiritual decency and order; upon an Ecclesiastical magistrality, and reverend infallibility, and prodigious policy; as if there were no way to bring a papist to church, but by going with him to Rome first; if we confide well we shall find cause enough to remove these as not consistent with the gospel of Jesus Christ; nor the spiritual purity; we were then trading with Popery, when we ignorantly thought, we had not enough to serve us from the Scriptures of God, now we see they are perfect and complete in Jesus Christ; let us cast away those weak and beggarly rudiments, of the which we are not ashamed and they do not favor the things of God, who would now (like Saul at Endor) raise up Aron, and the whole hierarchy, and bring us back again to the law, forcing us under that cool shadow, of types and ceremonies, drawing the curtains of the law before the light of the Gospel; we serve now in the newness of the spirit, not in the deadness of the letter. 

The next thing I observe, is the political excellency, those found maxims for the kingdoms duration, as the preservation of parliamentary rights, and national rights, and royal rights, that they exceed not, nor exorbiate; and this is no more then to reduce the kingdom into its primitive contemperation, and to keep the mixture even; for we see that while the power of monarchy would needs take such poignant principles, as the divines and privates did instill, carrying up the notion of a king into an higher firmament than its own,the whole state was in a posture of ruin; for nothing hath more betrayed kingdoms into destructive alteration than exorbitancy of government; and states must respect  their fundamentals and originals in their present Constitution; for those in secret dispositions into all the orders and subordinations, and that engagement enables to incline to their own interests; and then in their inclinations and pursuit thither, tumults and stirres are wakened, and there is a noise of some new and treasonable endeavor; when it is but the natural workings of each degree for its own preservation and hence have these lat aspersions been born and received, the princes and people being carried on by the strength and violence of a late predominancy, almost out of sight of their true fundamentals; till they have forgot their interests, and malign those that would bring them back; and for the temper of our English monarchy, I will not say more to the praise of the constitution, so many have spoken before me) than its own duration, breathing to this day, under the succession of so many ages, and never distempered, but when the fiduciary power would needs be sever and taller than the rest, and entertain designs of trying strengths and interests, to see "If the fire out of the bramble would at length come forth, and consume the trees of the forest." And whereas there are some other subordinate principles in order to these, as the discovery of incendiaries and malignants; certainly they that shall suffer any relations to corrupt them to secrecy, are men of too narrow affections for the latitude of a kingdom; they are only in the reputation of patriots and fidelio's to their country, who have such a command of spirit, as they can open and close as the occasion that the state requires. 
    And I do further observe, that we are obliged to mutual preservation of the peace of the kingdoms, and in special of the Reformation of the church in Scotland. For the first, it is built upon the foundation of our government, which is a concentration of the three estates; in one; and there is such a famine almost in the fundamentals of the three, and there is such a monarchical oneness which influences into all, that if any alteration being in any one of them, it will soon like an infection (where there is consanguinity of nature) spread and make over to the rest; so that there is much policy in suffering our care and faithfulness to enlarge and acquaint itself with the interest of the other estates, which are in no other sense foreign; than only in the distance of place.
    And for the particular preservation of the church of Scotland, it is as concerning an interest as any of the rest; what godly soul will repine to take up the care of another Church; he was (we know) a sanguinary man that replied, "Am I my brothers keeper?" And we may take notice there how God calls for an account of every relation at our hands. It was an holy principle that persuaded the Apostle "To take care of all the Churches"; and the more near we approach to such endeavors, the more near we are to the designs and activity of God and Angels, whose business and administration is universal; and especially should our care be for that Church and State, which hath been the conservatory of the Gospel, and kept alive that holy spark, which we in this kingdom do warm our souls by at this day; she was that "Philadelphia who kept the Word of his patience." Nor let any complain of restraints in this our Covenant, as if we multiplied unnecessary oaths, as if like "Sauls" oath it had "troubled the land", there is no such true liberty, as in these holy restraints; nor is it any diminutio to our Christian Latitude, that we cannot transgress nor exceed in this or that; it is rather the sublimation of our liberty, and a deliverance into the "glorious liberty of the son's of God"; and as it is the highest perfection, "non posse peccare", not to be able to transgress, so our estate and condition in this our Covenant, is a degree to that, for God, Angels, and Saints, are not less perfect, nor less free, because they cannot sin, but is a confirmation of soul in the height of holiness, for as it is the highest aggravation of sin, not to be able to do good, so it is the highest perfection of the goodness not to be able to do ill; so that I know none that hath a spiritual and discerning soul, that will complain for want of liberty to transgress; or that he is in heaven before his time; such chains are but, chains of god, nay, but the bracelets of the soul, as as it is the devil's and reprobates torment and misery to be held in chains of darkness, so it is the glory of the blessed saints and angels to be held in such chains of light and holiness; and none but "Libertines" will complain, that they are walled in, and that their lusts have not liberty to anger God, and undo their Country. 
    I observe another transcendency in it, there have been many Covenant's taken by the people of God, "Josiah's, Jehosphapahts, and Nehemiah's, and the Foreign Protestations" and those of our own, yet none that hath such a Spiritual and Political breadth in it, reaching not only to nations, but all particulars; and taking in the furthest and most foreign necessity, and circumstance either in state or in church. 
    I might take in other particulars, but they are such as clear up to your first discovery, only my thoughts have rolled up themselves into this conclusion. Since the Covenant is of this transcedency and excellency, to solemn and sacred, it were fit there were some holy design, to work it more close to the souls of those that take it, all our "happiness spiritual and civil" is now in the success of our confederation with our God, and therefore there would be as much hold art used in preserving the spirits of people in that height of Covenanting, as there was used in the raising them up; God himself conservation as much his business as the creation of the world, and therefor some make it the same act repeated again.
    I should think it not unnecessary, that those parts in it which have most of the attestations, and invocation, and implication, and most of the political and civil advantages, be set on by ministry in frequent inculcation, even to a "Catechizing", and by a civil ordinance, or law to that purpose to remind the ministry, for we complain of looseness and neglect in former Covenants, and we take no care to inquire into the reasons, and remedies; and certainly the sudden laying aside such rational and obligatory doctrines and making them bu the music and solemnity of one day, is the only reason of our relaxations, and apostasies, when having got the souls of men into Covenanting Nation, we should apply our strengths and honest designs to keep them there. 

A Divine Rapture upon the Covenant
------------------------------------

Children of Zion, rise, and sit not on 
Those flowery banks of Babylon,
Her streams are muddy and 
impure, and know
Her channel's bloody where
they flow.
Oh let us to a Region, where
we may
Bath in pure waters every day,
Waters of Life, and happiness,
which have
A Chrystal Grace in 
every
waves
We all make ready to be gone,
and mean 
Never to see those banks again
Oh stay not, till heaven scourge 
you with a rod
Into the city of your God,
See here a chain of Pearl, and
watery dew
Wept from the side of God
for you;
See here a chain of Rubies
from each wound,
Let down in Purple to
the ground;
Come see your hearts with
ours, to make one Ring,
And shred them on our golden
string;
Great God, let down some
glorious beam of thine,
To wind about his soul and
mine,
And every ones; then we shall
joyfully be,
Made sure to heaven and Thee.

FINIS



